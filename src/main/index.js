'use strict'

import { shell, app, BrowserWindow, Menu, Tray } from 'electron'
/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static')
}

let mainWindow

const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

const MenuTray = Menu.buildFromTemplate([
  {
    label: 'Afficher l\'application fréquence country',
    click: () => {
      mainWindow.show()
    }
  },
  {
    label: 'Ouvrir le site officiel de la radio',
    click: () => {
      shell.openExternal('https://frequencecountry.mirandais.fr')
    }
  },
  {
    label: 'Accéder à notre page facebook',
    click: () => {
      shell.openExternal('https://www.facebook.com/Frequence-Country-862322540540302/')
    }
  },
  {
    label: 'Fermer la radio',
    click: () => {
      app.isQuiting = true
      app.quit()
    }
  },
  {
    label: 'ping',
    click: () => {
      mainWindow.webContents.send('ping')
    }
  }
])

const Browser = {
  width: 1000,
  height: 500,
  icon: './static/img/icon.png',
  title: 'Fréquence country',
  maximized: false,
  center: true,
  frame: false
}

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})

app.on('close', function (event) {
  if (!app.isQuiting) {
    event.preventDefault()
    mainWindow.hide()
  }
  return false
})

app.on('minimize', function (event) {
  event.preventDefault()
  mainWindow.hide()
})

function createWindow () {
  // creation de la fenetre
  mainWindow = new BrowserWindow(Browser)
  // Bloquer l'agrandissement de la fenêtre
  mainWindow.setResizable(false)
  // chargement de la page par default
  mainWindow.loadURL(winURL)
  // suppression du menu par defaut
  mainWindow.setMenu(null)
  // cacher l'application de la barre de tache
  mainWindow.setSkipTaskbar(true)
  // mainWindow.webContents.openDevTools()
  // barre de notification
  const tray = new Tray('./static/img/icons.png')
  // affichage du nom de l'application
  tray.setToolTip('Fréquence Country: l\'application')
  // création du menu de l'icone
  tray.setContextMenu(MenuTray)
}
