import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'

Vue.use(VueRouter)

const router = new VueRouter({
  // mode: 'history',
  // History mode won't work in production
  // https://simulatedgreg.gitbooks.io/electron-vue/content/en/renderer-process.html#notice
  routes: [{
    path: '/',
    component: require('./components/Actu/Actu.vue'),
    name: 'root'
  }, {
    path: '/actualite/:id([0-9]*)-([a-z]*)',
    component: require('./components/Actu/Actu.vue'),
    name: 'actualite'
  }, {
    path: '/actualite/',
    component: require('./components/Actu/Actu.vue'),
    name: 'actualite'
  }, {
    path: '/media/videotheque',
    component: require('./components/Media/Media.vue'),
    name: 'videotheque'
  }, {
    path: '/media/phototheque',
    component: require('./components/Media/Media.vue'),
    name: 'phototheque'
  }, {
    path: '*',
    redirect: '/'
  }]
})

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  template: '<App/>'
}).$mount('#app')
